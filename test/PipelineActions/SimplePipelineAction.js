var chai = require('chai');
var chaiAsPromised = require("chai-as-promised");
var sinon = require('sinon');

chai.use(chaiAsPromised);
chai.should();

var action = require('../../saver/PipelineActions/SimplePipelineAction');

var clock;

describe('SimplePipelineAction.doStuff', function () {

	before(function () {
		clock = sinon.useFakeTimers();
	});

	after(function () {
		clock.restore();
	});

	it('should get a number', function () {
		var testAction = action();

		var result = testAction.doStuff(1);
		clock.tick(2000);
		return result.should.eventually.be.a('number');
	});

	it('should increment given number', function () {
		var testAction = action();

		var result = testAction.doStuff(1);
		clock.tick(2000);
		return result.should.eventually.equal(2);
	});

	it('should reject too high number', function () {
		var testAction = action();

		var result = testAction.doStuff(4);
		clock.tick(2000);
		return result.should.eventually.be.rejected;
	});
});