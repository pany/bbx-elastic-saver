var chai = require('chai');
var chaiAsPromised = require("chai-as-promised");
var sinon = require('sinon');

chai.use(chaiAsPromised);
chai.should();

var pipeline = require('../../saver/Pipeline/Pipeline');

describe('Pipeline.addAction', function () {
	it('should add given action to empty pipeline', function () {
		var testAction = 1;
		var testPipeline = pipeline();

		testPipeline.should.haveOwnProperty('_actions');
		testPipeline._actions.should.be.a('array');
		testPipeline._actions.should.have.length(0);

		testPipeline.addAction(testAction);

		testPipeline._actions.should.eql([1]);
	});

	it('should add given action to filled pipeline', function () {
		var testAction = 2;
		var testPipeline = pipeline();

		testPipeline.should.haveOwnProperty('_actions');
		testPipeline._actions.should.be.a('array');

		testPipeline._actions = [1];

		testPipeline._actions.should.have.length(1);

		testPipeline.addAction(testAction);

		testPipeline._actions.should.eql([1, 2]);
	})
});

describe('Pipeline.process', function () {
	it('with empty pipeline should return unchanged item', function () {
		var testPipeline = pipeline();
		var item = 1;

		return testPipeline.process(item).should.eventually.eql(item);
	});

	it('with one action should call action on given item', function () {
		var testPipeline = pipeline();
		var item = 1;

		var testMethod = sinon.stub();
		testMethod.withArgs(1).returns(2);
		testMethod.returns(0);

		var testAction = {
			doStuff: testMethod
		};

		testPipeline._actions = [testAction];

		return testPipeline.process(item).should.eventually.equal(2);
	});

	it('with multiple actions should call all actions on given item', function () {
		var testPipeline = pipeline();
		var item = 1;

		var firstMethod = sinon.stub();
		firstMethod.withArgs(1).returns(2);
		firstMethod.returns(0);

		var firstAction = {
			doStuff: firstMethod
		};

		var secondMethod = sinon.stub();
		secondMethod.withArgs(2).returns(3);
		secondMethod.returns(0);

		var secondAction = {
			doStuff: secondMethod
		};

		testPipeline._actions = [firstAction, secondAction];

		return testPipeline.process(item).should.eventually.equal(3).then(function () {
			firstMethod.calledBefore(secondMethod).should.be.true;
			firstMethod.calledOnce.should.be.true;
			secondMethod.calledOnce.should.be.true;
		});
	});

	it('with multiple actions one reject cancels remaining actions', function () {
		var testPipeline = pipeline();
		var item = 1;

		var firstMethod = sinon.stub();
		firstMethod.withArgs(1).returns(2);
		firstMethod.returns(0);

		var firstAction = {
			doStuff: firstMethod
		};

		var rejectMethod = sinon.stub();
		rejectMethod.withArgs(2).returns(Promise.reject());
		rejectMethod.returns(0);

		var rejectAction = {
			doStuff: rejectMethod
		};

		var secondMethod = sinon.stub();
		secondMethod.withArgs(2).returns(3);
		secondMethod.returns(0);

		var secondAction = {
			doStuff: secondMethod
		};

		testPipeline._actions = [firstAction, rejectAction, secondAction];

		// return testPipeline.process(item).should.eventually.be.rejected;
		return testPipeline.process(item).should.eventually.be.rejected.then(function () {
			firstMethod.calledOnce.should.be.true
			firstMethod.calledBefore(rejectMethod).should.be.true;
			rejectMethod.calledOnce.should.be.true;
			secondMethod.called.should.be.false;
		});
	});

});
