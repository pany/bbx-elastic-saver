
var chai = require('chai');
var chaiAsPromised = require("chai-as-promised");
var sinon = require('sinon');

chai.use(chaiAsPromised);
chai.should();


var loop = require('../../saver/Loop/Loop.js');
var stub = sinon.stub();


describe('loop.run', function () {

	it('should end on last steps', function () {
		var runMethod = sinon.stub().returns(Promise.resolve());
		var step = {
			run: runMethod
		};
		var remainingSteps = 0;

		var realLoop = loop();
		var mockedLoop = sinon.mock(realLoop).expects("nextIteration").never();

		realLoop.run(step, remainingSteps);

		mockedLoop.verify();
		runMethod.called.should.be.false;
	});

	it('should call run on given step', function () {
		var runMethod = sinon.stub().returns(Promise.resolve());
		var step = {
			run: runMethod
		};
		var remainingSteps = 1;

		var realLoop = loop();

		realLoop.run(step, remainingSteps);

		runMethod.calledOnce.should.be.true;
	});

	it('should reject when step rejects', function () {
		var runMethod = sinon.stub().returns(Promise.reject());
		var step = {
			run: runMethod
		};
		var remainingSteps = 1;

		var realLoop = loop();
		var mockedLoop = sinon.mock(realLoop).expects("nextIteration").never();

		realLoop.run(step, remainingSteps);

		mockedLoop.verify();
		runMethod.calledOnce.should.be.true;
	});

	it('should call nextIteration after given successful step', function () {
		var runMethod = sinon.stub();
		runMethod.returns(Promise.resolve('prvni'));
		var step = {
			run: runMethod
		};
		var remainingSteps = 1;

		var realLoop = loop();

		var nextIterationMethod = sinon.spy();
		realLoop.nextIteration = nextIterationMethod;

		return realLoop.run(step, remainingSteps).should.become(undefined).then(function () {
			nextIterationMethod.calledOnce.should.be.true;
			nextIterationMethod.calledWith(step, remainingSteps).should.be.true;
			runMethod.calledOnce.should.be.true;
		});
	})
});

describe('loop.nextIteration', function () {
	it('should call run with decreased remaining step count', function () {
		var clock = sinon.useFakeTimers();

		var runMethod = sinon.stub();
		runMethod.returns(Promise.resolve('prvni'));
		var step = {};
		var remainingSteps = 1;

		var realLoop = loop();

		var runMethod = sinon.spy();
		realLoop.run = runMethod;

		realLoop.nextIteration(step, remainingSteps);

		// force event loop to process async next iteration
		clock.tick(1);

		runMethod.calledOnce.should.be.true;
		runMethod.calledWith(step, 0).should.be.true;

		clock.restore();
	})
});
