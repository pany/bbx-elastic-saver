var chai = require('chai');
var chaiAsPromised = require("chai-as-promised");
var sinon = require('sinon');

chai.use(chaiAsPromised);
chai.should();


var step = require('../../saver/Loop/Step.js');
var stub = sinon.stub();


describe('step.run', function () {
	it('should check if loop function is present', function () {
		chai.assert(typeof step == 'function', 'loop is not a function');
	});

	it('passes the args to step', function() {
		var queueItem = 1;
		var processedItem = 2;

		var confirmMethod = sinon.stub();
		confirmMethod.withArgs(processedItem).returns(undefined);
		confirmMethod.returns(0);

		queue = {
			pop: sinon.stub().returns(queueItem),
			confirm: confirmMethod
		};

		pipeline = {
			process: sinon.stub().withArgs(queueItem).returns(processedItem)
		};

		var testStep = step([queue, pipeline]);
		return testStep.run().should.become(undefined, 'Item from queue was unexpectedly altered').then(
			function () {
				queue.pop.calledOnce.should.be.true;
				pipeline.process.calledOnce.should.be.true;
				queue.confirm.calledOnce.should.be.true;
			}
		);
	});

	it('does not process nor confirm item from failed queue pop', function () {
		var queueItem = 1;
		var processedItem = 2;

		var confirmMethod = sinon.stub();
		confirmMethod.withArgs(processedItem).returns(undefined);
		confirmMethod.returns(0);

		queue = {
			pop: sinon.stub().returns(Promise.reject()),
			confirm: confirmMethod
		};

		pipeline = {
			process: sinon.stub().withArgs(queueItem).returns(processedItem)
		};

		var testStep = step([queue, pipeline]);
		return testStep.run().should.be.rejected.then(
			function () {
				queue.pop.calledOnce.should.be.true;
				pipeline.process.called.should.be.false;
				queue.confirm.called.should.be.false;
			}
		);
	});

	it('does not confirm item from failed processing', function () {
		var queueItem = 1;
		var processedItem = 2;

		var confirmMethod = sinon.stub();
		confirmMethod.withArgs(processedItem).returns(undefined);
		confirmMethod.returns(0);

		queue = {
			pop: sinon.stub().returns(queueItem),
			confirm: confirmMethod
		};

		pipeline = {
			process: sinon.stub().withArgs(queueItem).returns(Promise.reject())
		};

		var testStep = step([queue, pipeline]);
		return testStep.run().should.be.rejected.then(
			function () {
				queue.pop.calledOnce.should.be.true;
				pipeline.process.calledOnce.should.be.true;
				queue.confirm.called.should.be.false;
			}
		);
	});
});
