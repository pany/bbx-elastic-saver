var assert = require('assert');

var parseMetas = require('../../xml_json/metas.js');

var mocks = require('../data/metas.js');

var logStack = [];

var log = function (string) {
	logStack.push(string);
};

beforeEach(function () {
	logStack = [];
});

describe('Metas Parser', function () {

	it('should parse metas correctly and not log anything', function () {
		assert.deepEqual(parseMetas({
			meta: mocks.valid
		}, {}, log), { guid: 'i:article', 'bbx-type': 'article' });
		assert.deepEqual(logStack, []);
	});

	it('should log when there are duplicate metas', function () {
		assert.deepEqual(parseMetas({
			meta: mocks.duplicate
		}, {}, log), { guid: 'i:article' });
		assert.deepEqual(logStack, ['Duplicate metas']);
	});

	it('should log when there is no content or name attrbute', function () {
		assert.deepEqual(parseMetas({
			meta: mocks.invalid.missingContent
		}, {}, log), { guid: 'i:article' });
		assert.deepEqual(parseMetas({
			meta: mocks.invalid.missingName
		}, {}, log), { guid: 'i:article' });
		assert.deepEqual(parseMetas({
			meta: mocks.invalid.general
		}, {}, log), { guid: 'i:article' });
		assert.deepEqual(logStack, ['Invalid meta', 'Invalid meta', 'Invalid meta']);
	});

	it('should log when there is a text node inside a meta', function () {
		assert.deepEqual(parseMetas({
			meta: mocks.withText
		}, {}, log), { guid: 'i:article' });
		assert.deepEqual(logStack, ['Has text in between metas (ignoring).']);
	});

	it('should log when there is an extra attribute or child node inside a meta', function () {
		assert.deepEqual(parseMetas({
			meta: mocks.withSubNode
		}, {}, log), { guid: 'i:article' });
			assert.deepEqual(parseMetas({
				meta: mocks.withUnknownAttribute
			}, {}, log), { guid: 'i:article' });
		assert.deepEqual(logStack, ['Has meta with unknown attribute(s) or children.', 'Has meta with unknown attribute(s) or children.']);
	});

});
