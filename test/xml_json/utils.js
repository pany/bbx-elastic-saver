var assert = require('assert');

var utils = require('../../xml_json/utils.js');

var TYPES = require('../../xml_json/types.js');

describe('Utils', function () {

	describe('isObject', function () {
		it('should return false when the value is null', function () {
			assert.strictEqual(utils.isObject(null), false);
		});

		it('should return false when the value is a primitive', function () {
			assert.strictEqual(utils.isObject('string'), false);
			assert.strictEqual(utils.isObject(1), false);
			assert.strictEqual(utils.isObject(false), false);
		});

		it('should return true when the value is an array', function () {
			assert.strictEqual(utils.isObject([]), true);
			assert.strictEqual(utils.isObject([0, 1, 2]), true);
		});

		it('should return true when the value is an object', function () {
			assert.strictEqual(utils.isObject({}), true);
			assert.strictEqual(utils.isObject(Math), true);
			assert.strictEqual(utils.isObject(new RegExp('.*')), true);
			assert.strictEqual(utils.isObject({
				prop: 'val'
			}), true);
		});
	});

	describe('isArray', function () {
		it('should return false when the value is null', function () {
			assert.strictEqual(utils.isArray(null), false);
		});

		it('should return false when the value is a primitive', function () {
			assert.strictEqual(utils.isArray('string'), false);
			assert.strictEqual(utils.isArray(1), false);
			assert.strictEqual(utils.isArray(false), false);
		});

		it('should return true when the value is an array', function () {
			assert.strictEqual(utils.isArray([]), true);
			assert.strictEqual(utils.isArray([0, 1, 2]), true);
		});

		it('should return false when the value is an object', function () {
			assert.strictEqual(utils.isArray({}), false);
			assert.strictEqual(utils.isArray(Math), false);
			assert.strictEqual(utils.isArray(new RegExp('.*')), false);
			assert.strictEqual(utils.isArray({
				prop: 'val'
			}), false);
		});
	});

	describe('getDefault', function () {
		it('should return an empty string when the value is TYPES.STRING', function () {
			assert.equal(utils.getDefault(TYPES.STRING), '');
		});

		it('should return an empty array when the value is TYPES.ARRAY', function () {
			assert.deepEqual(utils.getDefault(TYPES.ARRAY), []);
		});

		it('should return an empty object when the value is TYPES.OBJECT', function () {
			assert.deepEqual(utils.getDefault(TYPES.OBJECT), {});
		});

		it('should return an empty string when the value is not recognized', function () {
			assert.equal(utils.getDefault('unknown'), '');
			assert.equal(utils.getDefault(true), '');
			assert.equal(utils.getDefault(null), '');
			assert.equal(utils.getDefault(undefined), '');
			assert.equal(utils.getDefault(1), '');
			assert.equal(utils.getDefault([]), '');
			assert.equal(utils.getDefault({}), '');
		});
	});
});
