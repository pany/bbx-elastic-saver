var assert = require('assert');

var parseRelations = require('../../xml_json/relations.js');

var mocks = require('../data/relations.js');

var logStack = [];

var log = function (string) {
	logStack.push(string);
};

beforeEach(function () {
	logStack = [];
});

describe('Relations Parser', function () {

	it('should parse relations correctly and not log anything', function () {
		assert.deepEqual(parseRelations({
			relation: mocks.valid
		}, {}, log), [{
			id: 'guid',
	    storage: 'storage',
	    type: 'tag',
	    site: null,
	    hidden: false,
	    internal: true,
	    content: 'name'
		}, {
			id: 'guid',
			storage: null,
	    type: 'tag',
			site: null,
			hidden: false,
			internal: false,
			content: ''
		}]);
		assert.deepEqual(logStack, []);
	});

	it('should log when there is no guid', function () {
		assert.deepEqual(parseRelations({
			relation: mocks.noGuid
		}, {}, log), []);
		assert.deepEqual(logStack, ['Has relation with empty GUID.', 'Has relation with empty GUID.']);
	});

	it('should log when there is nollist and nolist', function () {
		var expectation = {
			id: 'guid',
	    storage: null,
	    type: undefined,
	    site: null,
	    hidden: true,
	    internal: false,
	    content: ''
		};

		assert.deepEqual(parseRelations({
			relation: mocks.nollist
		}, {}, log), [expectation]);
		assert.deepEqual(parseRelations({
			relation: mocks.nollistWithNolist
		}, {}, log), [expectation]);
		assert.deepEqual(logStack, ['Has relation with nollist and nolist attribute.']);
	});

	it('should log when there is an invalid nolist or internal attrbibute', function () {
		var expectation = {
			id: 'guid',
	    storage: null,
	    type: undefined,
	    site: null,
	    hidden: false,
	    internal: false,
	    content: ''
		};

		assert.deepEqual(parseRelations({
			relation: mocks.invalidNolist
		}, {}, log), [expectation]);
		assert.deepEqual(parseRelations({
			relation: mocks.invalidInternal
		}, {}, log), [expectation]);
		assert.deepEqual(logStack, [
			'Has relation with nolist set to something other then "on"/"off" yes (leaving default value).',
			'Has relation with internal set to something other then "on"/"off" yes (leaving default value).'
		]);
	});

});
