var assert = require('assert');

var parse = require('../../xml_json/generic.js');

var mocks = require('../data/generic.js');

describe('Generic Parser', function () {

	it('should not modify primitives', function () {
		var expectation = Object.assign({}, mocks.primitives);
		var result = parse(mocks.primitives);

		assert.deepEqual(result, expectation);
	});

	it('should trim strings', function () {
		assert.deepEqual(parse(mocks.trim), 'trim');
	});

	it('should convert undefined to default value', function () {
		assert.deepEqual(parse(mocks.notDefined, 'string'), '');
		assert.deepEqual(parse(mocks.notDefined, 'array'), []);
		assert.deepEqual(parse(mocks.notDefined, 'object'), {});
	});

	it('should convert array with one item into proper array', function () {
		assert.deepEqual(parse(mocks.arraySingle), [{
			node: {
				_: 'node',
				attr: 'value'
			}
		}]);
	});

	it('should parse array ommitting array tag', function () {
		assert.deepEqual(parse(mocks.arrayFull), [{
			node: 'yes', next: 'no'
		}, {
			node: 'yes', next: 'no'
		}, {
			node: 'yes', next: 'no'
		}]);
	});

	it('should parse guid', function () {
		assert.deepEqual(parse(mocks.guid), {
			guid: 'guid',
			title: 'name',
			site: 'site',
			storage: 'storage'
		});
	});

	it('should add storage to guid without one', function () {
		assert.deepEqual(parse(mocks.guidNoStorage), {
			guid: 'guid',
			title: 'name',
			site: 'site',
			storage: null
		});
	});

});
