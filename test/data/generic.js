module.exports = {
	primitives: {
		string: 'value',
		number: 10,
		boolean: true,
		'dashed-string': 'value',
		empty: '',
	},
	trim: '\ttrim\n\r\t',
	notDefined: undefined,
	arraySingle: {
		array: {
			node: {
				_: 'node',
				attr: 'value'
			}
		},
	},
	arrayFull: {
		array: [{
			node: 'yes', next: 'no'
		}, {
      node: 'yes', next: 'no'
		}, {
      node: 'yes', next: 'no'
		}]
	},
	guid: {
		guid: { _: 'guid', storage: 'storage' },
		title: 'name',
		site: 'site'
	},
	guidNoStorage: {
		guid: 'guid',
		title: 'name',
		site: 'site'
	}
};
