module.exports = {
	valid: [{
		guid: 'guid',
    storage: 'storage',
    reltype: 'tag',
    site: null,
    hidden: 'off',
    internal: 'on',
    _: 'name'
	}, {
		guid: 'guid',
    reltype: 'tag'
	}],
  noGuid: [{
	}, {
		guid: '',
	}],
	nollist: [{
		guid: 'guid',
		nollist: 'on',
	}],
	nollistWithNolist: [{
		guid: 'guid',
		nollist: 'on',
		nolist: 'on'
	}],
	invalidNolist: [{
		guid: 'guid',
		nolist: 'yes'
	}],
	invalidInternal: [{
		guid: 'guid',
		internal: 'yes'
	}]
};
