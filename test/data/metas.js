module.exports = {
	valid: [{
		content: 'i:article', name: 'guid'
	}, {
		content: 'article', name: 'bbx-type'
	}],
	invalid: {
		missingContent: [{
			name: 'guid'
		}, {
			content: 'i:article', name: 'guid'
		}],
		missingName: [{
			content: 'i:article'
		}, {
			content: 'i:article', name: 'guid'
		}],
		general: [{
			some: 'thing'
		}, {
			content: 'i:article', name: 'guid'
		}]
	},
	duplicate: [{
		content: 'i:article', name: 'guid'
	}, {
		content: 'i:article', name: 'guid'
	}],
	withText: [{
		content: 'i:article', name: 'guid', _: 'textContent'
	}],
	withSubNode: [{
		content: 'i:article', name: 'guid', subnode: 'subnode'
	}],
	withUnknownAttribute: [{
		content: 'i:article', name: 'guid', attr: 'value'
	}]
};
