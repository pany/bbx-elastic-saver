module.exports = `<item>
<metas>
<meta content="i:article:101420" name="guid">text</meta>
<meta content="article" name="bbx-type"><span>a</span></meta>
<meta content="101420" name="othersystemid"/>
<meta content="aktualne:article" name="othersystemtype"/>
<meta content="publish" name="status"/>
<meta content="2006-03-11 19:00:01" name="pubtime"/>
<meta content="" name="copyright"/>
<meta content="2016-03-02 12:38:45" name="migrate"/>
<meta content="" name="fastfilter"/>
<meta content="Washingtonská smlouva" name="title"/>
<meta content="washingtonska-smlouva" name="slug"/>
<meta content="2016-03-02 12:38:45" name="changetime"/>
<meta content="2" name="revision"/>
</metas>
<head>
<listing-object/>
<annotation></annotation>
<title>Washingtonská smlouva</title>
<seotitle></seotitle>
<updated>off</updated>
</head>
<body>
<primary-object/>
<control><show-ads>on</show-ads><show-discussion>	on </show-discussion></control>
<authors/>
<yes>no</yes>
<custom><array><node>yes</node><next>no</next></array><array><node>yes</node><next>no</next></array><array><node>yes</node><next>no</next></array></custom>
<vsections><array><guid storage="XY">b:vs:zena</guid><title>žena</title><site>b:site:zena</site></array></vsections>
<tags><array><guid storage="XY">b:tag:zenacz</guid><title>Žena.cz</title></array><array><guid>i:keyword:432</guid><title>NATO</title></array><array><guid>i:keyword:553</guid><title>smlouva</title></array></tags>
<references/>
<content>&lt;p&gt;&lt;strong&gt;Washington D.C., 4.dubna 1949&lt;/strong&gt;&lt;/p&gt;&#13;
&lt;p&gt;Strany této smlouvy znovu potvrzují svoji víru v cíle a zásady Charty OSN a svoji touhu žít v míru se všemi národy a všemi vládami.&lt;/p&gt;&#13;
&lt;p&gt;Jsou odhodlány hájit svobodu, společné dědictví a kulturu svých národů, založenou na zásadách demokracie, svobody jednotlivce a právního řádu.&lt;/p&gt;&#13;
&lt;p&gt;Jejich snahou je podporovat stabilitu a blahobyt národů v severoatlantickém prostoru.&lt;/p&gt;&#13;
&lt;p&gt;Jsou rozhodnuty spojit své úsilí o kolektivní obranu a zachování míru a bezpečnosti.&lt;/p&gt;&#13;
&lt;p&gt;Proto se dohodly na této Severoatlantické smlouvě.&lt;/p&gt;&#13;
&lt;p&gt;&lt;strong&gt;ČLÁNEK 1&lt;/strong&gt;&lt;BR&gt;Smluvní strany se zavazují, jak je uvedeno v Chartě OSN, urovnávat veškeré mezinárodní spory, v nichž mohou být účastny, mírovými prostředky tak, aby nebyl ohrožen mezinárodní mír, bezpečnost a spravedlnost, a zdržet se ve svých mezinárodních vztazích hrozby silou nebo použití síly jakýmkoli způsobem neslučitelným s cíli OSN&lt;/p&gt;&#13;
&lt;p&gt;&lt;strong&gt;ČLÁNEK 2&lt;/strong&gt;&lt;BR&gt;Smluvní strany budou přispívat k dalšímu rozvoji mírových a přátelských mezinárodních vztahů posilováním svých svobodných institucí, usilováním o lepší porozumění zásadám, na nichž jsou tyto instituce založeny, a vytvářením podmínek pro stabilitu a blahobyt. Budou usilovat o vyloučení z konfliktu ze své mezinárodní hospodářské politiky a budou podporovat hospodářskou spolupráci mezi všemi smluvními stranami jednotlivě nebo společně.&lt;/p&gt;&#13;
</content>
<test><![CDATA[This is inside CDATA <element></element>]]></test>
</body>
<relations>
<relation guid="b:site:zena" reltype="site" nolist="off"/>
<relation nollist="off" nolist="off" guid="b:vs:zena" reltype="section" site="b:site:zena">žena</relation>
<relation nolist="off" gui="b:tag:zenacz" reltype="tag"> Žena.cz</relation>
<relation nolist="on" guid="i:keyword:432" reltype="tag">NATO</relation>
<relation nolist="off" guid="i:keyword:553" reltype="tag">
			smlouva
	   </relation>
</relations>
</item>`;
