var Container = require('./saver/Container/Container');
var config = require('./saver/Config/Config');

var container = new Container(config);
container.getApp().run(process.argv[2]);

