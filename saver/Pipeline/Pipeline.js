function pipeline() {
	var object = Object.create(pipeline.proto);
	object._actions = [];
	return object;
}

pipeline.proto = {
	addAction: function (action) {
		this._actions.push(action);
	},
	process: function (item) {
		return this._actions.reduce(function(prev, cur) {
			return prev.then(cur.doStuff);
		}, Promise.resolve(item));
	}
};

module.exports = pipeline;
