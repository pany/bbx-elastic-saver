function step(args) {
	var object = Object.create(step.proto);
	object.queue = args[0];
	object.pipeline = args[1];
	return object;
}

step.proto = {
	run: function () {
		var _this = this;
		return Promise.resolve(this.queue.pop())
			.then(this.pipeline.process.bind(_this.pipeline))
			.then(this.queue.confirm);
	}
};


module.exports = step;
