var step = require('./Step');

function stepFactory() {
	return Object.create(stepFactory.proto);
}

stepFactory.proto = {
	createStep: function (name, args) {
		switch (name) {
			case 'processQueue':
				return step(args);
		}
	}
};

module.exports = stepFactory;
