function loop() {
	return Object.create(loop.proto);
}

loop.proto = {
	run: function (step, remaining) {
		if (remaining !== null && remaining <= 0) {
			return;
		}
		var _this = this;
		return step.run().then(this.nextIteration.bind(_this, step, remaining));
	},
	nextIteration: function (step, remaining) {
		var _this= this;
		setImmediate(this.run.bind(_this) , step, --remaining);
	}
};

module.exports = loop;
