var exports= module.exports = {};

exports.elaconect = function() {
  var elasticsearch = require('elasticsearch');
  return new elasticsearch.Client({
    host: 'bbx-elastic-import.aws.isrv.cz:9200',
    log: 'warning'
   });
 }

exports.mysqlconnect = function() {
  var mysql = require("mysql");
  var con = mysql.createConnection({
     host: "localhost",
     user: "pany",
     password: "",
     database: 'bbx_content'
  });

   con.connect(function(err){
       if(err){
         console.log('Error connecting to Db');
         return;
       }
       console.log('Connection established');
     });

    return con;
 }

exports.redisconnect = function () {
   var redis = require('redis');
   return redis.createClient();

 }
