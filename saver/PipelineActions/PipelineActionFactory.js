var simplePipelineAction = require('./SimplePipelineAction');
var logPipelineAction = require('./LogPipelineAction');

function pipelineActionFactory() {
	return Object.create(pipelineActionFactory.proto);

}

pipelineActionFactory.proto = {
	createAction: function (name, args) {
		switch (name) {
			case 'logPipelineAction':
				return logPipelineAction();
				break;
			case 'simplePipelineAction':
				return simplePipelineAction();
				break;
			default:
		}
	}

};

module.exports = pipelineActionFactory;
