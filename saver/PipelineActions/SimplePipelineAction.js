
function simplePipelineAction() {
	return Object.create(simplePipelineAction.proto);
}

simplePipelineAction.proto = {
	doStuff: function (val) {
		return new Promise(
			function(res, rej) {
				if (val > 3) {
					rej('moc velka');
				}
				else {
					setTimeout(function(){
							res(++val)
						},
						1000
					);
				}
			}
		);
	}
};

module.exports = simplePipelineAction;
