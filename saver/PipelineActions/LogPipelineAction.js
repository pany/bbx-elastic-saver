
function logPipelineAction() {
	return Object.create(logPipelineAction.proto);
}

logPipelineAction.proto = {
	doStuff: function(item) {
		console.log(item);
		return item;
	}
};

module.exports = logPipelineAction;
