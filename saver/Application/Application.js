function application(container) {
	var object = Object.create(application.proto);
	object._container = container;
	return object;
}

application.proto = {
	run: function (maxSteps) {
		var container = this._container;

		var queue = container.getService('queue');

		var pipeline = container.getService('pipeline');

		var pipeActionFactory = container.getService('pipelineActionFactory');
		var simpleAction = pipeActionFactory.createAction('simplePipelineAction');
		var logAction = pipeActionFactory.createAction('logPipelineAction');

		pipeline.addAction(simpleAction);
		pipeline.addAction(logAction);

		var stepFactory = container.getService('stepFactory');
		var step = stepFactory.createStep('processQueue', [queue, pipeline]);
		var loop = container.getService('loop');

		loop.run(step, maxSteps);
	}
};

module.exports = application;
