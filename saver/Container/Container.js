var application = require('../Application/Application');
var queue = require('../Queue/Queue');
var pipeline = require('../Pipeline/Pipeline');
var pipelineActionFactory = require('../PipelineActions/PipelineActionFactory');
var loop = require('../Loop/Loop');
var stepFactory = require('../Loop/StepFactory');


function container(config) {
	var object = Object.create(container.proto);
	object._config = config;
	object._services = {
		'queue': null,
		'pipeline': null,
		'pipelineActionFactory': null,
		'loop': null,
		'stepFactory': null,
		'redis' :  null,
		'mysql' : null,
		'elasticsearch' : null
	};
	return object;
}

container.proto = {
	getService: function (name) {
		if (this._services.hasOwnProperty(name)) {
			if (this._services[name] === null) {
				this._services[name] = this.createService(name);
			}
			return this._services[name];
		}
		else {
			throw 'Unknown service ' + name;
		}
	},

	getApp: function () {
		return application(this);
	},

	createService: function (name) {
		switch(name) {
			case 'queue':
				return queue();
			case 'pipeline':
				return pipeline();
			case 'pipelineActionFactory':
				return pipelineActionFactory();
			case 'loop':
				return loop();
			case 'stepFactory':
				return stepFactory();
			case 'redis':
				return this.config.redisconnect();
			case 'mysql':
				return this.config.mysqlconnect();
			case 'elasticsearch':
				return this.config.elaconect();
			default:
		}
	}
};

module.exports = container;
