// defaults.js

'use strict';

const TYPES = require('./types.js');

module.exports = {
	storage: null,
	head: {
		'listing-object': TYPES.ARRAY,
		seotitle: TYPES.STRING,
		subtitle: TYPES.STRING,
		shorttitle: TYPES.STRING,
	},
	body: {
		'primary-object': TYPES.ARRAY,
		admins: TYPES.ARRAY,
		authors: TYPES.ARRAY,
		crops: TYPES.ARRAY,
		'tracking-code': TYPES.STRING,
		sections: TYPES.ARRAY,
		references: TYPES.ARRAY,
		tags: TYPES.ARRAY,
		vsections: TYPES.ARRAY,
		control: TYPES.ARRAY,
		slides: [{
			guid: TYPES.STRING,
			type: TYPES.STRING,
			subtype: TYPES.STRING,
			title: TYPES.STRING,
			'slide-parts': [{
				guid: TYPES.STRING,
				title: TYPES.STRING,
				content: TYPES.STRING,
				photo: TYPES.STRING,
				'photo-crop': TYPES.STRING,
				'photo-size': TYPES.STRING,
			}],
		}],
		correspondents: TYPES.ARRAY,
		online: {
			'complex-title': {
				'home-team': TYPES.STRING,
				'home-shortname': TYPES.STRING,
				'home-team-url': TYPES.STRING,
				'home-team-goals': TYPES.STRING,
				'visiting-team': TYPES.STRING,
				'visiting-shortname': TYPES.STRING,
				'visiting-team-url': TYPES.STRING,
				'visiting-team-goals': TYPES.STRING,
				'league-name': TYPES.STRING,
				round: TYPES.STRING,
				score: TYPES.STRING,
				'score-parts': TYPES.STRING,
				'home-team-image': {
					guid: TYPES.STRING,
					description: TYPES.STRING,
				},
				'visiting-team-image': {
					guid: TYPES.STRING,
					description: TYPES.STRING,
				},
			},
			'simple-title': TYPES.STRING,
			'match-start': TYPES.STRING,
			results: TYPES.STRING,
			medals: TYPES.STRING,
			'custom-icons': TYPES.ARRAY,
		},
	},
};
