'use strict';

const DEFAULT_STORAGE = require('./defaults').storage;
const TYPES = require('./types.js');
const parseGeneric = require('./generic.js');

module.exports = function (relations, defaults, log) {
	return relations.relation.map(function (relation) {
		if (!relation.guid) {
			return log('Has relation with empty GUID.'), null;
		}

		if ('nollist' in relation) {
			if ('nolist' in relation) {
				log('Has relation with nollist and nolist attribute.');
			}
			relation.nolist = relation.nollist;
		}

		['nolist', 'internal'].forEach(function (key) {
			if (key in relation && typeof relation[key] !== 'string') {
				throw new Error(`Unexpected relation attribute ${key} value`);
			}
			if (key in relation && relation[key] !== 'on' && relation[key] !== 'off') {
				log(`Has relation with ${key} set to something other then "on"/"off" ${relation[key]} (leaving default value).`);
				delete relation[key];
			}
		});

		return {
			id: relation.guid,
			storage: relation.storage || DEFAULT_STORAGE,
			type: relation.reltype,
			site: relation.site || null,
			hidden: relation.nolist === 'on',
			internal: relation.internal === 'on',
			content: parseGeneric(relation._, TYPES.STRING)
		};
	}).filter(function (relation) {
		return relation;
	});
}
