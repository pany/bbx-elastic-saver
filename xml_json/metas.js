'use strict';

const utils = require('./utils.js');

module.exports = function (metas, defaults, log) {
	let invalid = false;

	const array = Object.assign.apply(Object, metas.meta.map(function (meta) {
		if (!meta || !utils.isObject(meta) || !('name' in meta) || !('content' in meta)) {
			invalid = true;
			return log('Invalid meta'), {};
		}

		const len = Object.keys(meta).length;
		if (len !== 2) {
			const _ = '_' in meta;
			if (_) {
				log('Has text in between metas (ignoring).');
			}
			if (len !== 3 || !_) {
				log('Has meta with unknown attribute(s) or children.');
			}
		}

		return {
			[meta.name]: meta.content
		};
	}));

	if (!invalid && metas.meta.length > Object.keys(array).length) {
		log('Duplicate metas');
	}

	return array;
}
