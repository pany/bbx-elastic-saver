// isObject.js

'use strict';

const TYPES = require('./types.js');

module.exports = {
	isObject: function (object) {
		return object !== null && typeof object === 'object'
	},
	isArray: function (array) {
		return Object.prototype.toString.call(array) === '[object Array]';
	},
	getDefault: function (type) {
		switch (type) {
			case TYPES.ARRAY:  return [];
			case TYPES.STRING: return '';
			case TYPES.OBJECT: return {};
			default:           return '';
		}
	}
};
