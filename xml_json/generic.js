'use strict';

const utils = require('./utils.js');
const DEFAULT_STORAGE = require('./defaults.js').storage;

function reduce(tree, defaults, log) {
	const reduced = tree;

	defaults = defaults || {};

	if (typeof reduced === 'string') {
		return reduced.trim() || utils.getDefault(defaults);
	}

	if (reduced === undefined) {
		return utils.getDefault(defaults);
	}

	if (typeof reduced === 'object' && reduced !== null) {
		for (const i in reduced) {
			if (i === 'array') {
				if (!utils.isArray(reduced.array)) {
					reduced.array = [reduced.array];
				}
				return reduced.array.map(function (item) {
					return reduce(item, defaults[0])
				});
			}

			if (i === 'guid' && utils.isObject(reduced.guid)) {
				reduced.storage = reduced.guid.storage || DEFAULT_STORAGE;
				reduced.guid = reduce(reduced.guid._, defaults.guid);
			} else {
				if (i === 'guid') {
					reduced.storage = DEFAULT_STORAGE;
				}
				reduced[i] = reduce(reduced[i], defaults[i]);
			}
		}
	}

	return reduced;
}

module.exports = reduce;
