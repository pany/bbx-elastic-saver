'use strict';

const TYPES = require('./types.js');
const DEFAULTS = require('./defaults.js');

const utils = require('./utils.js');
const Parser = require('xml2js').Parser;
const parseMetas = require('./metas.js');
const parseGeneric = require('./generic.js');
const parseRelations = require('./relations.js');

const FUNCTION_MAP = {
	metas: parseMetas,
	head: parseGeneric,
	body: parseGeneric,
	relations: parseRelations
};

let GUID;

function log() {
	var log = Array.prototype.slice.call(arguments);
	log.unshift(GUID, ':');
	console.log.apply(console, log);
}

function toArray(node, guid) {
	if (!utils.isObject(node) || !utils.isObject(node.item)) {
		throw new Error('WTF');
	}

	const item = node.item;
	const keys = {};

	GUID = guid;

	let key;

	for (key in item) {
		if (key === '$' || key === '_') {
			continue;
		}
		if (!(key in FUNCTION_MAP)) {
			return log(`Unknown child ${key} node.`), undefined;
			continue;
		}
		if (key in keys) {
			return log(`Has multiple ${key} nodes.`), undefined;
		}
		keys[key] = true;
		item[key] = FUNCTION_MAP[key](item[key], DEFAULTS[key], log);
	}

	if (key === undefined) {
		return log('Has no child nodes.'), undefined;
	}

	for (key in item.body) {
		switch (key) {
			case 'metas':
			case 'head':
			case 'body':
			case 'relations':
				return log(`Can not unpack body because it contains key ${key}.`), undefined;
			case 'relation':
			case 'meta':
			case 'heads':
				log(`Body contains singular/plural ${key} of reserved key.`);
			default:
				item[key] = item.body[key];
				break;
		}
	}

	delete item.body;

	return item;
}

module.exports = function (data, callback) {
	const parser = new Parser({
		attrkey: '$',
		charkey: '_',
		mergeAttrs: true,
		explicitArray: false
	});

	parser.parseString(data.xml, function (err, result) {
		if (err) {
			return callback(err);
		}
		callback(null, toArray(result, data.guid));
	});
};
