// index.js

'use strict';

module.exports = {
	ARRAY: 'array',
	STRING: 'string',
	OBJECT: 'object'
};
