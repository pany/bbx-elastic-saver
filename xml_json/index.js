// index.js
'use strict';

const Convertor = require('./convertor.js');

const testXml = require('../test/data/xml.js');

const util = require('util');

Convertor({
	guid: 'test',
	xml: testXml
}, function () {
	console.log(
		util.inspect(
			arguments, false, null
		)
	)
});
